package co.com.redhat.sso.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class Util {
	
	public static XMLGregorianCalendar getDateTime() throws DatatypeConfigurationException {
		 GregorianCalendar gcal = new GregorianCalendar();
	     return  DatatypeFactory.newInstance()
	            .newXMLGregorianCalendar(gcal);
		 
	}
	
	public static String getIpAdrress() throws UnknownHostException {
		InetAddress inetAddress = InetAddress.getLocalHost();
		return inetAddress.getHostAddress();
	}

}
